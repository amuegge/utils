from setuptools import setup

setup(
    name='lpp',
    version='0.0.1',
    py_modules=['lpp'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        lpp=lpp:lpp
    ''',
)