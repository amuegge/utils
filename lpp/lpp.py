import click
import os
import subprocess
from pyfzf import FzfPrompt
import socket
import re
import yaml

with open(os.environ["HOME"]+"/.lpp.yml", "r") as f:
    port_mappings = yaml.load(f, Loader=yaml.FullLoader)

def get_resource_list(type):
    process = subprocess.run(['kubectl', 'get', type, '--namespace=preprod', '--no-headers'], 
                             stdout=subprocess.PIPE, 
                             universal_newlines=True)
    
    return process.stdout.split("\n")

def choose_resource(type):
    fzf = FzfPrompt()

    choice = fzf.prompt(get_resource_list(type))
    choice_attributes = list(filter(None, choice[0].split(" ")))

    return [type]+choice_attributes


def check_socket(port):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        s.bind(("127.0.0.1", port))
        s.close()
        return True
    except socket.error as msg:
        click.echo("Port {} is unusable".format(port))
        return False
	

def check_pulse_secure_interface():
    click.echo("checking pulse...")
    out = subprocess.run(['ifconfig'], capture_output=True, text=True).stdout
    if(not re.search("^utun.*POINTOPOINT.*\n\s+inet 10.", out, re.MULTILINE)):
        click.echo(out)
        click.echo(click.style("... no utun found.", fg='red'))
        return False

    click.echo("... found utun")
    return True


def save_mapping(type, resource, port):
    with open(os.environ["HOME"]+"/.lpp.yml", "w") as f:
        if(port_mappings[type] is None):
            port_mappings[type] = { resource: port}
        else:
            port_mappings[type][resource] = port

        yaml.dump(port_mappings, f, default_flow_style=True)


def get_mapping(type, resource):
    try:
        # click.echo(str(port_mappings[type]))
        return port_mappings[type][resource]
    except:
        return None

def forward_port(service_attrs):
    type = service_attrs[0]
    res = service_attrs[1]

    if("services" == type):
        remote_port = service_attrs[5]
        remote_port = re.sub("[/]TCP$", "", remote_port)
    else:
        remote_port = 8989
        
    valid=False
    while(not valid):
        default = get_mapping(type, res)
        if(default is not None):
            click.echo("this {} was previously mapped to {}".format(type, default))

        local_port = click.prompt("Local port to use for {}:{}".format(res, remote_port), type=int, default=default)
        if(local_port < 1024):
            click.echo(click.style("port must be >1024", fg='red'))
        else:
            valid = check_socket(local_port)


    click.echo("mapping preprod/{}/{}:{} to localhost:{}".format(type, res, remote_port, local_port))
    save_mapping(type, res, local_port)

    gke_prefix = {"services": "svc/", "pods": "pod/"}[type]
    process = subprocess.Popen(["kubectl",
                                "port-forward",
                                "--context",
                                "gke_np-digital-enterprise_us-west1_enterprise-active",
                                "--namespace", 
                                "preprod",
                                gke_prefix+res,
                                str(local_port)+":"+str(remote_port)

                            ], 
                            stdout=subprocess.PIPE,
                            env=dict(os.environ),
                            universal_newlines=True)


    while True:
        output = process.stdout.readline()
        click.echo(output.strip())

        return_code = process.poll()
        if return_code is not None:
            click.echo('kubectl return code {}'.format(return_code))
            for output in process.stdout.readlines():
                click.echo(output.strip())
            break


@click.group()
def lpp():
    pass

@click.command()
def pod():
    if(not check_pulse_secure_interface()):
        raise Exception("Can't find any vpn tunnels - is pulse running?")
    forward_port(choose_resource("pods"))

@click.command()
def service():
    if(not check_pulse_secure_interface()):
        raise Exception("Can't find any vpn tunnels - is pulse running?")
    forward_port(choose_resource("services"))

lpp.add_command(pod)
lpp.add_command(service)
