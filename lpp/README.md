this is a tool to forward ports to atb's preprod gke services & pods.

it requires fzf (https://github.com/junegunn/fzf), get that using 'brew install fzf'.

it also requires you to have preprod access in gke, and to access that gke cluster you need to be on pulse.


Getting Started 
---

	pipenv sync
	pipenv run lpp service

		this will list the running services in gke preprod, typing will filter matches.  enter key selects a
		service, which will prompt you for a local port number.  enter the port number and 
		connections on that port will be forwarded to the selected service.  typical failure scenarios are
		prevented and port choice is saved and supplied later as a default based on service name.

	pipenv run lpp pod

		as above, but for pods.  port 8989 is assumed.
