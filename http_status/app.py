from flask import Flask, request
from flasgger import Swagger
from http import HTTPStatus
import time

app = Flask(__name__)
swagger = Swagger(app)

@app.route('/status/<rescode>', methods=['GET', 'POST', 'PATCH', 'DELETE'])
def status(rescode=None):
    """return a result with the specified status
    This is using docstrings for specifications.
    ---
    parameters:
      - name: rescode
        in: path
        type: string
        required: true
        default: 200
    responses:
      200:
        description: http response with the supplied rescode (if valid) and a descriptive body for that code
      500:
        description: error for invalid/unparsable rescode argument values
    """

    rescode = int(rescode)
    delay = request.args.get("delay")

    if(HTTPStatus(rescode) is None):
        return ""

    if(delay is not None):

        time.sleep(int(delay))
    return f"{rescode} {HTTPStatus(rescode).phrase}", rescode

if __name__ == '__main__':
    app.debug = True
    app.run(host="0.0.0.0")
