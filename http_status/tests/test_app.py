import timeit

import app
import pytest


@pytest.fixture
def client():
    with app.app.test_client() as c:
        yield c


def test_ok(client):
    r = client.get("/status/200")
    assert r.status_code == 200


def test_404(client):
    r = client.get("/status/404")
    assert r.status == "404 NOT FOUND"
    assert r.status_code == 404


def test_invalid_700(client):
    r = client.get("/status/700")
    # pprint.pprint(vars(r))
    assert r.status == "500 INTERNAL SERVER ERROR"  # NOT A VALID STATUS CODE


def test_timeout(client):
    start = timeit.default_timer()
    r = client.get("/status/200?delay=20")
    assert timeit.default_timer() - start > 20
    assert timeit.default_timer() - start < 21  # should be in the millis + 20s
    assert r.status == "200 OK"
