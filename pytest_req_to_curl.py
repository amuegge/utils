#!/usr/local/bin/python3
import os, sys

class reqObj:
    method = None
    url = None
    headers = []
    body = []

    state = 0
    state_got_method_url = 1
    state_got_headers = 2
    state_got_body = 3
    state_ended = 4

    def strip_preamble(self, l):
        l = l.rstrip()
        if(l.startswith("> ")):
            return l[2:]
        return l

    def parse_method_url(self, l):
        parts = self.strip_preamble(l.rstrip()).split()
        self.method = parts[0]
        self.url = parts[1]
        self.state = 1

    def parse_header(self, l):
        self.headers.append(self.strip_preamble(l));
        if(l.startswith("> Content-Length")):
            self.state = 2

    def parse_body(self, l):
        self.body.append(self.strip_preamble(l))

    def parse_line(self, l):
        # print "$ "+l
        if(l.rstrip() == "Request:"):
            return
        if(not l.startswith("> ")):
            return

        if(self.state == 0):
            self.parse_method_url(l)
        elif(self.state == 1):
            self.parse_header(l)
        elif(self.state == 2):
            self.parse_body(l)
        elif(self.state == 4):
            raise "Parsing past end of body!"
        
        # print self.state, " ", self

    def to_curl(self):
        print("curl -X "+self.method+" "+self.url+" \\")
        for h in self.headers:
            # account for any length added by CURL_OPTS by stripping explicit content length
            if(not "Content-Length" in h):
                print(' -H "'+h+'" \\')
        if("CURL_OPTS" in os.environ): 
                print(os.environ["CURL_OPTS"]+" \\")
        print("--data '"+"".join(self.body)+"'")

r = reqObj()
for l in sys.stdin:
    r.parse_line(l)
r.to_curl()
